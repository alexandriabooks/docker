################### NPM BUILD IMAGE ###################
FROM node:12-alpine as js-build-image


WORKDIR /src/
RUN npm config set @alexandriabooks:registry https://gitlab.com/api/v4/packages/npm/ && \
    wget $(npm view @alexandriabooks/hifi dist.tarball) && \
	tar -zxf *.tgz

################### FINAL IMAGE ###################
FROM python:3.9 as final-image

ENV PYTHONUNBUFFERED=1
EXPOSE 8080

RUN apt update -qq && \
	apt install -y -qq --no-install-recommends build-essential gcc cmake curl \
		libffi-dev libssl-dev libarchive-dev libxml2-dev libxslt1-dev \
		libtiff5-dev libjpeg62-turbo-dev libopenjp2-7-dev zlib1g-dev \
		libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev \
		python3-tk libharfbuzz-dev libfribidi-dev libxcb1-dev \
		libyaml-dev libtag1-dev ffmpeg > /dev/null && \
	rm -rf /var/lib/apt/lists/*

RUN useradd -ms /bin/bash alexandria && \
    mkdir /work && \
    chown alexandria /work

RUN pip install alexandriabooks-core --extra-index-url https://gitlab.com/api/v4/projects/24158886/packages/pypi/simple && \
	pip install alexandriabooks-apiserver --extra-index-url https://gitlab.com/api/v4/projects/24167931/packages/pypi/simple && \
	pip install alexandriabooks-formats --extra-index-url https://gitlab.com/api/v4/projects/24166989/packages/pypi/simple && \
	pip install alexandriabooks-acefile --extra-index-url https://gitlab.com/api/v4/projects/24168024/packages/pypi/simple && \
	pip install alexandriabooks-ffmpeg --extra-index-url https://gitlab.com/api/v4/projects/24167959/packages/pypi/simple && \
	pip install alexandriabooks-libarchive --extra-index-url https://gitlab.com/api/v4/projects/24167968/packages/pypi/simple && \
	pip install alexandriabooks-local --extra-index-url https://gitlab.com/api/v4/projects/24168036/packages/pypi/simple && \
	pip install alexandriabooks-pdf2image --extra-index-url https://gitlab.com/api/v4/projects/24168069/packages/pypi/simple && \
	pip install alexandriabooks-pillow --extra-index-url https://gitlab.com/api/v4/projects/24168066/packages/pypi/simple && \
	pip install alexandriabooks-taglib --extra-index-url https://gitlab.com/api/v4/projects/24259196/packages/pypi/simple

COPY --from=js-build-image /src/package/build /opt/alexandria-hifi

ENV PATH="/opt/alexandria/bin:$PATH"

USER alexandria
WORKDIR /work

ENV ALEXANDRIABOOKS_WEB_PORT=8080
ENV ALEXANDRIABOOKS_WEB_HIFI_PATH=/opt/alexandria-hifi
ENV ALEXANDRIABOOKS_CACHE_PLUGIN=alexandriabooks_local.cache.FileBasedCacheService
ENV ALEXANDRIABOOKS_CACHE_DIRECTORY=/work/cache
ENV ALEXANDRIABOOKS_LOCAL_DIRECTORY=/work/books

ENTRYPOINT ["python", "-m", "alexandriabooks_core"]
CMD ["run"]
